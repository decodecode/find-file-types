#!/usr/bin/env coffee
# Find which file types in current diretory and below.

{basename,extname}=require 'path'
walk=require 'walkdir'

# What to look for, collect.
count_dirs=count_files=0
types={}

walk process.argv[2] ?'.' # Directory as 1st CLI argument, default to current.
.on 'link',->types.symlink=1+(types.symlink ?0) # Ignore (presumably internal) symlinks.
.on 'directory',->count_dirs+=1
.on 'file',(path,s)->
	count_files+=1

	# Known types.
	rules=
		'Make':(fn,ext)->fn in ['Makefile','makefile','GNUmakefile'] or ext is '.mk'
		'boilerplate':(fn)->fn.startsWith('boilerplate') or fn.startsWith('custom-boilerplate')
		'text':(fn)->
			fn in ['OWNERS','OWNERS_ALIASES','SECURITY_CONTACTS','AUTHORS','MAINTAINERS','CONTRIBUTORS','PATENTS','VERSION','COPYRIGHT','CODEOWNERS','GO_LICENSE','SUBMITTING_PATCHES'] or
			fn.startsWith('COPYING-') or
			fn.toUpperCase().startsWith('LICEN') or
			fn.toUpperCase().startsWith('README') or
			fn.toUpperCase().startsWith('NOTICE') or
			fn.toUpperCase().startsWith('CHANGELOG')
		'Bazel':(fn,ext)->fn in ['BUILD','.bazelrc'] or ext is '.bzl'
		'protobuf binary':(fn,ext)->ext is '.pb'
		'shell script':(fn,ext)->ext in ['.sh','.bash']
		'YAML/TOML/JSON':(fn,ext)->ext in ['.yaml','.yml','.toml','.json','.jsonl']
		'Markdown':(fn,ext)->ext in ['.md','.mdown','.markdown']
		'translations':(fn,ext)->ext in ['.mo','.po','.pot']
		'graphics (PNG/JPEG/GIF/SVG/favicon)':(fn,ext)->
			ext in ['.png','.gif','.jpeg','.jpg','.ico','.svg']
		'Git':(fn,ext)->
			fn in ['.gitignore','.gitattributes','HEAD','master','index','description','exclude','COMMIT_EDITMSG','ORIG_HEAD']
		'certificate':(fn,ext)->
			fn in ['cert','key'] or ext in ['.pem','.crt','.cer','.csr','.key']
		'Docker':(fn,ext)->fn in ['Dockerfile','BASEIMAGE','Dockerfile_windows']

		# Unrecognized and no extension — show file name instead.
		'':undefined

	fn=basename path
	ext=extname path # Extension as type?
	t=undefined

	# Search for match in rules.
	typer=(t,fn,ext)->
		if t isnt ''
			if rules[t] fn,ext then t else undefined
		else
			if ext then ext else fn
	for r of rules when not t
		t=typer r,fn,ext

	# Count.
	types[t]=1+(types[t] ?0)

.on 'end',->
	# Findings.
	console.log 'Subdirectories:',count_dirs
	console.log 'Files:',count_files
	console.log 'Files types:'
	by_count=([c,t] for t,c of types)
	for t in by_count.sort (x,y)->y[0]-x[0]
		console.log '',t[0],'\t',t[1]

	#? Verify logic: print(sum([x[0] for x in by_count]),'=',count_files)
# ? .on 'error'
