#!/usr/bin/env python3
# Find which file types in current diretory and below.

import os
from os import path

# What to look for, collect.
count_dirs=count_files=0
types={}

for root,dirs,files in os.walk('.'):
	# ? c['dirs'].extend([path.join(root,d) for d in dirs])
	count_dirs+=len(dirs)
	count_files+=len(files)

	for fn in files:
		_,t=path.splitext(fn) # Extension as type?

		# Ignore (presumably internal) symlinks.
		if path.islink(path.join(root,fn)): t='symlink'

		# Known?
		elif fn in ['Makefile','makefile','GNUmakefile'] or t=='.mk': t='Make'
		elif fn.startswith('boilerplate') or fn.startswith('custom-boilerplate'): t='boilerplate'
		elif fn in ['OWNERS','OWNERS_ALIASES','SECURITY_CONTACTS','AUTHORS','MAINTAINERS','CONTRIBUTORS','PATENTS','VERSION','COPYRIGHT','CODEOWNERS','GO_LICENSE','SUBMITTING_PATCHES']: t='text'
		elif fn.startswith('COPYING-'): t='text'
		elif fn.upper().startswith('LICEN'): t='text'
		elif fn.upper().startswith('README'): t='text'
		elif fn.upper().startswith('NOTICE'): t='text'
		elif fn.upper().startswith('CHANGELOG'): t='text'
		elif fn in ['BUILD','.bazelrc'] or t=='.bzl': t='Bazel'
		elif t=='.pb': t='protobuf binary'
		elif t in ['.sh','.bash']: t='shell script'
		elif t in ['.yaml','.yml','.toml']: t='YAML/TOML/JSON'
		elif t in ['.md','.mdown','.markdown']: t='Markdown'
		elif t in ['.json','.jsonl']: t='YAML/TOML/JSON'
		elif t in ['.mo','.po','.pot']: t='translations'
		elif t in ['.png','.gif','.jpeg','.jpg','.ico','.svg']: t='graphics (PNG/JPEG/GIF/SVG/favicon)'
		elif fn in ['.gitignore','.gitattributes','HEAD','master','index','description','exclude']: t='Git'
		elif fn in ['cert','key'] or t in ['.pem','.crt','.cer','.csr','.key']: t='certificate'
		elif fn in ['Dockerfile','BASEIMAGE','Dockerfile_windows']: t='Docker'

		# Unrecognized and no extension — show file name instead.
		elif t=='': t=fn

		# Count.
		if t not in types: types[t]=0 # Initialize.
		types[t]+=1

# Findings.
print('Subdirectories:',count_dirs)
print('Files:',count_files)
print('File types:')
by_count=[(c,t) for t,c in types.items()] # Transpose tuples.
for x in sorted(by_count,key=(lambda x:x[0]),reverse=True):
	print('',x[0],'\t',x[1])

#? Verify logic: print(sum([x[0] for x in by_count]),'=',count_files)
